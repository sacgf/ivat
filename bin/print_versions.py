#!/usr/bin/env python3

"""
Save versions to a csv file, used by VariantGrid
"""

import json
import sys
sys.dont_write_bytecode = True
import argparse
import pandas as pd
from sacgf.variant_and_gene_annotation_pipeline_parameters import VERSIONS

def get_command_line_options():
    parser = argparse.ArgumentParser(
            description=('Saves versions of annotation files to a csv file.'))
    parser.add_argument('-o', '--outcsv', 
            help='CSV output file.')
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = get_command_line_options()

    print(json.dumps(VERSIONS))

    if args.outcsv:
        pd.DataFrame.from_dict(VERSIONS, orient='index').to_csv(args.outcsv, sep=',', header=False)
