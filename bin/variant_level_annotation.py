#!/usr/bin/env python3

"""
Variant level annotation. 

When running on local machines, this script relies on the shell environment variable $SACGF_DIR

Input is a .vcf or .vcf.gz (bgzipped and tabixed) file. Output is .vcf.gz (bgzipped and tabixed).

By default, it adds everything including dbSNP IDs.

"""

import argparse
import cyvcf2
import gzip
import pickle
from pybedtools import BedTool

from sacgf import variant_and_gene_annotation_pipeline_parameters
from sacgf.util.file_utils import random_file_at_same_dir, random_file_name, absolute_file_path
from sacgf.util.subprocess_utils import run_and_handle_error, run_and_handle_returncode
from sacgf.util.vcf_utils import  remove_vcf_file, tabix_on_bgzipped_vcf, create_bgzip_tabix_vcf, \
        count_header_lines, extract_header_lines, validate_vcf_file, \
        append_new_info, get_info_tags, get_the_first_variant, \
        get_outhandle_with_new_headers, get_vcf_iterator_in_plain_text, having_contig_in_header, \
        sort_vcf_by_igvtools, count_variant_number, are_contigs_in_karyotypic_order
from sacgf.variant_and_gene_annotation_pipeline_parameters import *


sys.dont_write_bytecode = True

PROG_VERSION = '%s (0.0)' % DATE_VERSION #annot_source_ver (annot_file_change_ver.code_change_ver)

#the same tags are also in the INFO section of the output VCF file
EFF_SELECTED_TAGS = [
                'EFFECT_SNPEFF', 'IMPACT_SNPEFF', 
                'CODON_CHANGE_SNPEFF', 'AA_CHANGE_SNPEFF',
                'PRTN_LEN_SNPEFF', 'GENE_ID_SNPEFF', 
                'TRANSCRIPT_ID_SNPEFF', 'CHOSEN_ALT_SNPEFF']
NON_MODIFIER_IMPACTS = ['HIGH', 'MODERATE', 'LOW']
AVAILABLE_ANNOTATIONS = [
                'dbSNP-ID', 'snpEFF', 'miRNA', 'COSMIC', 'clinVar',
                'LCR', 'segment-dup', 'ESP', 'ExAC', 'gnomAD', 'nonTCGA-ExAC', '1KG', 'UK10K',
                'AA-effects', 'phyloP', 'GERP', 'CADD', 'TGI-tier', 'branch-point', 'Tier']

ENSEM_ID_TO_HGNC_MAPPING = pickle.load(open(ENSEMBL_TO_HGNC_MAPPING_FILE, "rb"))
CANONICAL_TSS = pickle.load(open(ENSEMBL_CANONICAL_TRANSCRIPTS_FILE, "rb"))

def get_command_line_options():
    parser = argparse.ArgumentParser(
            description=('Adds variant level annotation. '
                    'By default, it adds everything including dbSNP IDs. '
                    'Use "--skip" or "--add" to control what you want. '
                    ))
    parser.add_argument('--version', action='version', version=PROG_VERSION)
    parser.add_argument('-i', '--invcf', required=True, dest='invcf',
            help='Input file. File name must end with either .vcf or .vcf.gz')
    parser.add_argument('-o', '--outvcf', required=True, dest='outfile',
            help='Output file, in vcf-bgzipped format. It must ends with .vcf.gz')
    parser.add_argument('--allEffOut', dest='all_eff_out',
            help='Output file to store all effect predictions on all transcripts SNPEFF used. '
            'It must ends with .vcf.gz')
    parser.add_argument('--skip', action='append', choices=AVAILABLE_ANNOTATIONS,
            help=('An annotation to avoid. Can be specified multiple times. '
                'All but specified items will be added. Case sensitive. '))
    parser.add_argument('--add', action='append', choices=AVAILABLE_ANNOTATIONS,
            help=('An annotation to add. Can be specified multiple times. '
                'Only specified items will be added. '
                'It disables the --skip option. Case sensitive. '))
    parser.add_argument('--debug', action='store_true', help=('Set this option if you do NOT want to '
        'remove temporary/intermediate files. For debugging purposes.'))
    parser.add_argument('--noMultiALT', action='store_true', help=('Set this option if there is '
        'no variant record with multiple ALT alleles in the INVCF. '
        'It will skip one unnecessary step and become faster.'))
    parser.add_argument('--javaXmx', default='5g', help='Set the JAVA Xmx value for running snpEFF, '
        'in the "Xg" form, '
        'where "X" can be any integer >= 2. '
        'The default is "5g", so 5 GB ram is allocated. This option is basically for machines with '
        'low ram. Setting it with greater than 5g often will not make it significantly faster.')
    args = parser.parse_args()

    if not (args.invcf.endswith('.vcf.gz') or args.invcf.endswith('.vcf')):
        parser.print_usage()
        sys.exit('invcf name must end with .vcf or .vcf.gz')

    if not args.outfile.endswith('.vcf.gz'):
        parser.print_usage()
        print()
        sys.exit('outfile name must end with .vcf.gz')
    if args.all_eff_out and (not args.all_eff_out.endswith('.vcf.gz')):
        parser.print_usage()
        print()
        sys.exit('For allEffOut, file name must end with .vcf.gz')

    # make sure javaXmx >= 2g
    javaXmx = args.javaXmx
    try:
        int_, g = int(javaXmx[:-1]), javaXmx[-1]
    except ValueError:
        print()
        parser.print_usage()
        sys.exit(
            'please set the javaXmx in the "Xg" form, where "X" is an integer. Your setting is %s' % javaXmx)
    if g != 'g':
        print()
        parser.print_usage()
        sys.exit(
            'please set the javaXmx in the "Xg" form, where "X" is an integer. Your setting is %s' % javaXmx)
    if int_ < 2:
        print()
        parser.print_usage()
        sys.exit('please set the javaXmx >= 2g. Your setting is %s' % javaXmx)
    return args

def format_and_save_all_snpeff_predicts(infile, snpeff_full_out, all_eff_out):
    """
    extract and save all predicts by SNPEFF based on all transcrips 
    """
    info_headers_to_add = '''
##INFO=<ID=EFFECT_SNPEFF,Number=1,Type=String,Description="The effect resulting from the current variant. Multiple items are listed with | as separator for all transctipts, in the order as in TRANSCRIPT_ID_SNPEFF">
##INFO=<ID=IMPACT_SNPEFF,Number=1,Type=String,Description="Impact resulting from the current variant [MODIFIER, LOW, MODERATE, HIGH]. Multiple items are listed with | as separator for all transctipts, in the order as in TRANSCRIPT_ID_SNPEFF">
##INFO=<ID=CODON_CHANGE_SNPEFF,Number=1,Type=String,Description="Old/New codon for the current variant. Multiple items are listed with | as separator for all transctipts, in the order as in TRANSCRIPT_ID_SNPEFF">
##INFO=<ID=AA_CHANGE_SNPEFF,Number=1,Type=String,Description="Old/New amino acid from the current variant (in HGVS style). Multiple items are listed with | as separator for all transctipts, in the order as in TRANSCRIPT_ID_SNPEFF">
##INFO=<ID=PRTN_LEN_SNPEFF,Number=1,Type=String,Description="The length of protein for protein coding transcript. Multiple items are listed with | as separator for all transctipts, in the order as in TRANSCRIPT_ID_SNPEFF">
##INFO=<ID=GENE_ID_SNPEFF,Number=1,Type=String,Description="Ensembl gene ID for the current variant. Multiple items are listed with | as separator for all transctipts, in the order as in TRANSCRIPT_ID_SNPEFF">
##INFO=<ID=TRANSCRIPT_ID_SNPEFF,Number=1,Type=String,Description="Transcript ID for the current variant. Multiple items are listed with | as separator">
##INFO=<ID=CHOSEN_ALT_SNPEFF,Number=1,Type=String,Description="The ALT allele corresponding to the EFFECT_SNPEFF. Multiple items are listed with | as separator for all transctipts, in the order as in TRANSCRIPT_ID_SNPEFF">
'''.strip().split('\n')

    all_eff_outhandle = get_outhandle_with_new_headers(infile, all_eff_out[:-3], *info_headers_to_add) #all_eff_out ends with 'vcf.gz'

    infile_handle = get_vcf_iterator_in_plain_text(infile)
    snpeff_full_handle = get_vcf_iterator_in_plain_text(snpeff_full_out)
    #extract and construct eff info for each record line
    for in_line, annotated_line in zip(infile_handle, snpeff_full_handle):
        all_elements = annotated_line.rstrip().split('\t')
        #chr_, pos, _, ref, alt = all_elements[:5]
        info = all_elements[7] # info is the annotated info
        # #test
        # chr_, pos = all_elements[:2]

        if info == '.':
            constructed_info = ''
        else:
            all_parsed_single_effs = parse_snpeff_annot(info, eff_only=True)
            if all_parsed_single_effs:
                # #test
                # if chr_ == '1' and pos == '807302':
                    # import ipdb; ipdb.set_trace()

                constructed_infos = []
                for index, tag in enumerate(EFF_SELECTED_TAGS):
                    effs = [one_parsed_effs[index] for one_parsed_effs in all_parsed_single_effs]
                    if index == 4: # for 'PRTN_LEN_SNPEFF'
                        effs = [str(eff) if eff else '' for eff in effs]
                    if any(effs):
                        constructed_infos.append( '%s=%s' %(tag, '|'.join(effs)) )
                constructed_info = ';'.join(constructed_infos)
            else:
                constructed_info = ''
        out_line = append_new_info(in_line, constructed_info)
        all_eff_outhandle.write(out_line)
    all_eff_outhandle.close()
    create_bgzip_tabix_vcf(all_eff_out[:-3])

def add_snpeff_annotation(infile, outdir, outfile, javaXmx='5g', all_eff_out=None, invcf=None, debug=False):
    """
    invcf can be either .vcf or .vcf.gz (bgzipped)
    outfile is .vcf
    For the patterns of the output of snpEFF, see examine_snpeff_output.py
    """
    java_Xmx = '-Xmx' + javaXmx
    snpeff_full_out = os.path.join(outdir, 'snpeff_full.%s.vcf' % random_file_name())
    snpeff_cmd = '''
    java {java_Xmx} -XX:ParallelGCThreads=1 \\
            -jar {SNPEFF_JAR} eff \\
            -formatEff \\
            -geneId -noStats \\
            -no-downstream -no-upstream -no-intergenic -no INTRAGENIC \\
            {SNPEFF_DATA} \\
            {infile} \\
            > {snpeff_full_out}
    '''.format(java_Xmx=java_Xmx, SNPEFF_JAR=SNPEFF_JAR, SNPEFF_DATA=SNPEFF_DATA,
                infile=infile, snpeff_full_out=snpeff_full_out)
    run_and_handle_returncode(snpeff_cmd)

    # with all_eff_out: save re-formatted VCF records based on all TSs to the original invcf
    if all_eff_out:
        format_and_save_all_snpeff_predicts(invcf, snpeff_full_out, all_eff_out)

    print('\nExtracting snpEFF annotation...\n')
    extract_snpeff_annotation(infile, snpeff_full_out, outfile)

    if not debug:
        remove_vcf_file(snpeff_full_out)

def get_snpeff_out_headers(infile, SNPEFF_HEADERS_FILE):
    infile_headers = extract_header_lines(infile)

    snpeff_headers = open(SNPEFF_HEADERS_FILE).readlines()
    snpeff_headers = [line for line in snpeff_headers if line.startswith('##INFO=')]
    
    out_headers = infile_headers[:-1] + snpeff_headers + infile_headers[-1:]
    return out_headers

def extract_snpeff_annotation(infile, snpeff_full_out, snpeff_extracted_out):
    # add annotation from snpeff_full_out
    # 1) add the most damaging effect, with choosing the most appropriate transcript
    # 2) add NextProt as an additional INFO tag
    # 3) add TF_binding_site_variant as additional INFO tag
    # 4) add LOF as addtional INFO tags (and NMD is not needed as LOF is always
    #     there when NMD is existing)
    # 5) add Fract_of_func_TSs

    extracted_outhandle = open(snpeff_extracted_out, 'w')

    #construct and write headers to extracted_outhandle
    out_headers = get_snpeff_out_headers(infile, SNPEFF_HEADERS_FILE)
    extracted_outhandle.writelines(out_headers)

    #extract and construct eff info for each record line

    snpeff_full_inhandle = open(snpeff_full_out)
    for _ in range(count_header_lines(snpeff_full_out)):
        next(snpeff_full_inhandle)
    infile_inhandle = gzip.open(infile) if infile.endswith('.vcf.gz') else open(infile)
    for _ in range(count_header_lines(infile)):
        next(infile_inhandle)

    #It takes 35 seconds (user time) to finish with 0.8 million variants and 6 samples, with the current method
    for annotated_line, orig_line in zip(snpeff_full_inhandle, infile_inhandle):
        #out_count += 1
        info = annotated_line.rstrip().split('\t')[7] # info is the annotated info
        if info == '.':
            extracted_outhandle.write(orig_line)
        else:
            all_parsed_single_effs, tf_str, nextprot_str, gene_lof_map = parse_snpeff_annot(info)
            if all_parsed_single_effs:
                # calculate the fraction of functional predictions.
                fract_of_func_TSs = calc_fract_of_func_tss(all_parsed_single_effs)
                func_str = 'Fract_of_func_TSs=%.2f' % fract_of_func_TSs if fract_of_func_TSs else \
                    'Fract_of_func_TSs=0'

                # select the one with the most damaging effect
                selected_parsed_eff = selected_eff_predict(all_parsed_single_effs)
                eff_strs = []
                for tag, element in zip(EFF_SELECTED_TAGS, selected_parsed_eff):
                    if element:
                        eff_strs.append('%s=%s' % (tag, element))
                        if tag == 'GENE_ID_SNPEFF':
                            hgnc = ENSEM_ID_TO_HGNC_MAPPING.get(element, '')
                            if hgnc:
                                eff_strs.append('HGNC_SNPEFF=%s' % hgnc)
                eff_strs = ';'.join(eff_strs)
                #[eff, impact, codon_change, aa_change, prtn_length, gene_id, transcript_id, eff_alt_number]
                #gene_lof_map = {gene_id:[lof, gene_symbol],  }
                selected_gene_id = selected_parsed_eff[5]
                if gene_lof_map:
                    try:
                        lof_str = 'Fract_of_LOF_TSs=' + gene_lof_map[selected_gene_id][0]
                    except KeyError:
                        if len(gene_lof_map) == 1:
                            lof_str = 'Fract_of_LOF_TSs=' + list(gene_lof_map.values())[0][0]
                        else:
                            ## for test
                            #print
                            #print orig_line 
                            #print
                            combined_strs = ['%s:%s' % (value[1], value[0]) for value in
                                    list(gene_lof_map.values())]
                            lof_str = 'Fract_of_LOF_TSs=' + '|'.join(combined_strs)
                else:
                    lof_str = ''
            else:
                eff_strs = ''
                func_str = ''
                lof_str = ''
            if tf_str:
                #tf_str =  (join by '|') 
                tf_str = 'TF_binding=' + tf_str
            else:
                tf_str = ''
            if nextprot_str:
                nextprot_str = 'NextProt=' + nextprot_str
            else:
                nextprot_str = ''
            formated_info_to_add = ';'.join([info for info in [eff_strs, func_str, lof_str, 
                nextprot_str, tf_str] if info])
            extracted_outhandle.write(append_new_info(orig_line, formated_info_to_add))
    extracted_outhandle.close()

def calc_fract_of_func_tss(all_parsed_single_effs):
    #[eff, impact, codon_change, aa_change, prtn_length, gene_id, transcript_id, eff_alt_number]
    if all_parsed_single_effs[0][1] not in ['MODERATE', 'HIGH']:# the first one is always the one with highest eff
        return 0

    func_count = 0
    for eff_predict in all_parsed_single_effs:
        impact = eff_predict[1]
        if impact == 'MODERATE' or impact == 'HIGH':
            func_count += 1
    return float(func_count) / len(all_parsed_single_effs)

def selected_eff_predict(all_parsed_single_effs):
    """
    select the most damaging one, if there are more than one:
        1) select the canonical transcript
        2) select the one with the longest transcript
    """

    if len(all_parsed_single_effs) == 1:
        return all_parsed_single_effs[0]

    #select the ones with the most damaging effect
    highest_impact = all_parsed_single_effs[0][1] # the first one is always the one with highest eff
    predicts_with_highest_impacts = [eff_predict for eff_predict in all_parsed_single_effs if 
            eff_predict[1] == highest_impact]
    if len(predicts_with_highest_impacts) == 1:
        return predicts_with_highest_impacts[0]

    #find the canonical one
    predicts_with_highest_impacts_canonical = [eff_predict for eff_predict in predicts_with_highest_impacts
            if eff_predict[6] in CANONICAL_TSS]
    if len(predicts_with_highest_impacts_canonical) == 1:
        return predicts_with_highest_impacts_canonical[0]
    else:
        if predicts_with_highest_impacts_canonical:
            predictions = predicts_with_highest_impacts_canonical
        else:
            #none are canonical; find the longest one among predicts_with_highest_impacts
            predictions = predicts_with_highest_impacts

        eff_4_predictions = [eff_predict[4] for eff_predict in predictions if eff_predict[4] is not None]
        if eff_4_predictions:
            max_length = max(eff_4_predictions)
            for eff_predict in predictions:
                if eff_predict[4] == max_length:
                    return eff_predict
        else:
            # previous Python2.7 behavior was to sort an array containing None
            # this would have set max element = None and then take the 1st element
            return predictions[0]
            

def parse_snpeff_annot(info, eff_only=False):
    """
    Extract those snpeff annotatoin that I need as shown in EFF_SELECTED_TAGS

    info example:
    AC=1;AF=0.083;AN=12;BaseQRankSum=-1.179e+00;ClippingRankSum=-7.620e-01;DP=134;FS=3.010;GQ_MEAN=61.67;GQ_STDDEV=1.86;MLEAC=1;MLEAF=0.083;MQ=60.00;MQ0=0;MQRankSum=-1.179e+00;NCC=0;QD=3.35;ReadPosRankSum=-7.620e-01;SOR=1.911;EFF=frameshift_variant(HIGH||tat/|p.Tyr1007fs/c.3021delT|1037|ENSG00000187800|protein_coding|CODING|ENST00000338302|24|1),frameshift_variant(HIGH||tat/|p.Tyr1007fs/c.3021delT|1037|ENSG00000187800|protein_coding|CODING|ENST00000292357|23|1),TF_binding_site_variant[MA0099.2:AP1](LOW||||||||||1),TF_binding_site_variant[MA0099.1:Cfos](LOW||||||||||1),sequence_feature[compositionally_biased_region:Pro-rich](LOW|||c.3021delT|1037|ENSG00000187800|protein_coding|CODING|ENST00000292357|22|1),sequence_feature[compositionally_biased_region:Pro-rich](LOW|||c.3021delT|1037|ENSG00000187800|protein_coding|CODING|ENST00000292357|21|1),sequence_feature[compositionally_biased_region:Pro-rich](LOW|||c.3021delT|1037|ENSG00000187800|protein_coding|CODING|ENST00000292357|20|1),sequence_feature[compositionally_biased_region:Pro-rich](LOW|||c.3021delT|1037|ENSG00000187800|protein_coding|CODING|ENST00000292357|19|1),sequence_feature[compositionally_biased_region:Pro-rich](LOW|||c.3021delT|1037|ENSG00000187800|protein_coding|CODING|ENST00000292357|23|1),sequence_feature[compositionally_biased_region:Pro-rich](LOW|||c.3021delT|1037|ENSG00000187800|protein_coding|CODING|ENST00000292357|18|1),downstream_gene_variant(MODIFIER||4132|||ENSG00000187800|retained_intron|CODING|ENST00000482505||1),non_coding_exon_variant(MODIFIER|||n.537delT||ENSG00000187800|retained_intron|CODING|ENST00000465101|3|1),non_coding_exon_variant(MODIFIER|||n.2749delT||ENSG00000187800|retained_intron|CODING|ENST00000469390|18|1)

    multiple TF_binding_site_variant can be in EFF tag
    multiple sequence_feature can be in EFF tag
    LOF=(LY75-CD302|ENSG00000248672|2|1.00),(LY75|ENSG00000054219|6|0.50)

    return all_parsed_single_effs, tf_str, nextprot_str, gene_lof_map:
        all_parsed_single_effs = \
                [[values of EFF_SELECTED_TAGS], [values of EFF_SELECTED_TAGS], ] #order not changed
        tf_str =   (join by '|') 
        nextprot_str (join by '|')
        gene_lof_map = {gene_id:[lof, gene_symbol], }
    """
    info_elements = info.split(';')
    all_eff_str = find_eff_values(info_elements)
    if all_eff_str is None or \
            all_eff_str.startswith('('):# no eff available when 'EFF=(MODIFIER||||||||||1|ERROR_CHROMOSOME_NOT_FOUND)'
        if eff_only:
            return None
        else:
            return None, None, None, None

    # extract effs, TF binding info, nextprot info from the EFF value
    all_single_effs = []
    all_tfs = []
    all_nextprots = []
    for eff in all_eff_str.split(','):
        if eff.startswith('sequence_feature['):
            all_nextprots.append(eff)
        elif eff.startswith('TF_binding_site_variant['):
            all_tfs.append(eff)
        else:
            all_single_effs.append(eff)

    #parse the all_single_effs
    all_parsed_single_effs = list(map(split_single_eff, all_single_effs))
    if eff_only:
        return all_parsed_single_effs
    
    #parse the all_tfs
    # all_tfs[0] == 'TF_binding_site_variant[MA0139.1:CTCF](MODIFIER||||||||||1)'
    #len('TF_binding_site_variant[') == 24
    if all_tfs:
        tf_tmp = [ tf[24:tf.index(']')] for tf in all_tfs]
        tf_str = '|'.join(set(tf_tmp))
    else:
        tf_str = None

    #parse the all_nextprots
    if all_nextprots:
        # all_nextprots[0] == 'sequence_feature[transmembrane_region:Transmembrane_region](LOW|||c.180A>G|305|ENSG00000186092|protein_coding|CODING|ENST00000335137|1|1)'
        # sequence_feature[topological_domain:Mitochondrial_intermembrane](LOW|||c.445A>G|648|ENSG00000160072|protein_coding|CODING|ENST00000308647|2|1)
        # sequence_feature[coiled-coil_region](LOW|||c.484G>C|194|ENSG00000160075|protein_coding|CODING|ENST00000291386|5|1)
        # len('sequence_feature[') == 17
        nextprot_set = set()
        for nextprot in all_nextprots:
            nextprot_set.add(nextprot[17: nextprot.index(']')])
        nextprot_str = '|'.join(nextprot_set)
    else:
        nextprot_str = None

    #parse the LOF values
    all_lof_values = find_lof_values(info_elements)
    if all_lof_values:
        #Format: 'Gene_Name | Gene_ID | Number_of_transcripts_in_gene | Percent_of_transcripts_affected'
        #(AGAP2|ENSG00000135439|4|0.25)
        #(RP11-571M6.15|ENSG00000257921|3|0.33),(TSFM|ENSG00000123297|11|0.09)
        gene_lof_map = {}
        for alof in all_lof_values.split(','):
            alof_elements = alof[1:-1].split('|')
            gene_lof_map[alof_elements[1]] = [alof_elements[-1], alof_elements[0]] #[lof, gene_symbol]
    else:
        gene_lof_map = None

    return all_parsed_single_effs, tf_str, nextprot_str, gene_lof_map

def split_single_eff(eff_str):
    """
    extract and return a list of the needed eff elements as in the same order in EFF_SELECTED_TAGS

    EFF_SELECTED_TAGS = [
                'EFFECT_SNPEFF', 'IMPACT_SNPEFF', 
                'CODON_CHANGE_SNPEFF', 'AA_CHANGE_SNPEFF',
                'PRTN_LEN_SNPEFF', 'GENE_ID_SNPEFF', 
                'TRANSCRIPT_ID_SNPEFF', 'CHOSEN_ALT_SNPEFF']
    """
    #Effect ( Effect_Impact | Functional_Class | Codon_Change | Amino_Acid_Change| Amino_Acid_Length | Gene_Name | Transcript_BioType | Gene_Coding | Transcript_ID | Exon_Rank  | Genotype_Number [ | ERRORS | WARNINGS ] )
    eff = eff_str[:eff_str.index('(')]
    eff_details = eff_str[len(eff)+1 : -1].split('|')
    impact = eff_details[0]
    #func_class = eff_details[1]
    codon_change = eff_details[2]
    aa_change = eff_details[3]
    prtn_length = eff_details[4] # empty if it is not a protein coding gene
    prtn_length = int(prtn_length) if prtn_length else None
    gene_id = eff_details[5]
    transcript_id = eff_details[8]
    eff_alt_number = eff_details[10]
    return [eff, impact, codon_change, aa_change, prtn_length, gene_id, transcript_id, eff_alt_number]
    #same order as in EFF_SELECTED_TAGS


def find_eff_values(info_elements):
    """
    return the EFF string of values by snpEFF
    snpEff 4.0d always writes annotation to the last three elements
    """
    try:
        if info_elements[-1].startswith('EFF='):
            return info_elements[-1][4:]
        elif info_elements[-2].startswith('EFF='):
            return info_elements[-2][4:]
        elif info_elements[-3].startswith('EFF='):
            return info_elements[-3][4:]
        else:
            return None
    except IndexError:
        return None

def find_lof_values(info_elements):
    """
    return the LOF string of values by snpEFF
    """
    try:
        if info_elements[-1].startswith('LOF='):
            return info_elements[-1][4:]
        elif info_elements[-2].startswith('LOF='):
            return info_elements[-2][4:]
        elif info_elements[-3].startswith('LOF='):
            return info_elements[-3][4:]
        else:
            return None
    except IndexError:
        return None

def make_add_list(adds, skips):
    """
    returns the adds which are the items that actually will be added by the script
    """
    if skips and adds:
        #print out a message if both --skip and --add were specified
        print('The --add option disables the --skip option. Only these items will be added: ',  adds)
        return adds
    elif adds:
        return adds
    elif skips:
        adds = [item for item in AVAILABLE_ANNOTATIONS if item not in skips]
        return adds
    return AVAILABLE_ANNOTATIONS

def remove_info_at_multi_alt_variants(infile, outfile, items_to_remove, debug):
    """
    use bcftools to remove ALT-allele-specific annotation 
    input is .vcf or .vcf.gz
    output is .vcf
    """
    bcftools_remove_str = ','.join(['INFO/'+tag for tag in items_to_remove])

    #create multi_alt_allele_vcf without ALT-allele-specific annotation, and single_alt_allele_vcf
    multi_alt_allele_vcf = random_file_at_same_dir(outfile, 'multi_alt_allele', 'vcf.gz')
    single_alt_allele_vcf = random_file_at_same_dir(outfile, 'single_alt_allele', 'vcf.gz')
    files_to_remove = [multi_alt_allele_vcf, single_alt_allele_vcf]

    cmd = """
    {BCFTOOLS} annotate --include 'N_ALT > 2 | (ALT !~ "*" & N_ALT =2)' -O z --remove {bcftools_remove_str} \\
            {infile} > {multi_alt_allele_vcf}
    """.format(BCFTOOLS=BCFTOOLS, bcftools_remove_str=bcftools_remove_str, 
            infile=infile, multi_alt_allele_vcf=multi_alt_allele_vcf)
    run_and_handle_returncode(cmd)
    tabix_on_bgzipped_vcf(multi_alt_allele_vcf)

    cmd = """
    {BCFTOOLS} view --exclude 'N_ALT > 2 | (ALT !~ "*" & N_ALT =2)' -O z \\
            {infile} > {single_alt_allele_vcf}
    """.format(BCFTOOLS=BCFTOOLS, bcftools_remove_str=bcftools_remove_str, 
            infile=infile, single_alt_allele_vcf=single_alt_allele_vcf)
    run_and_handle_returncode(cmd)
    tabix_on_bgzipped_vcf(single_alt_allele_vcf)

    #Concatenate multi_alt_allele_vcf and single_alt_allele_vcf
    cmd = """
    {BCFTOOLS} concat --allow-overlaps {single_alt_allele_vcf} {multi_alt_allele_vcf} > \\
            {outfile}
    """.format(BCFTOOLS=BCFTOOLS, single_alt_allele_vcf=single_alt_allele_vcf,
            multi_alt_allele_vcf=multi_alt_allele_vcf, outfile=outfile)
    run_and_handle_returncode(cmd)

    if not debug:
        for f in files_to_remove:
            remove_vcf_file(f)

def check_needed_db_files_existing():
    # not necessarily needed for variant level annotation
    skip_vars = ('DATE_VERSION SNPEFF_DATA GATK_JAR MARK_LISTED_GENES_IN_CSV_CMD '
                'ENSEMBL_KEY_FULL_ANNOTATION_FILE CHRIS_ONE_PLUS_LIST CHRIS_TWO_PLUS_LIST').split()
    #TODO: also use types to judge if that is a module or function
    all_needed_vars = [v for v in dir(variant_and_gene_annotation_pipeline_parameters) 
                       if '__' not in v and v[0].isupper() and v not in skip_vars]

    non_existing_needed_files = {}
    all_variable_value_map = {k: v for k,v in globals().items() if '__' not in k}
    for var in all_needed_vars:
        value = all_variable_value_map[var]
        if type(value) is str:
            if os.path.isdir(value) or os.path.isfile(value):
                continue
            else:
                non_existing_needed_files[var] = value

    if len(non_existing_needed_files) > 0:
        print('\nERROR: These files or directories need to be existing:')
        for k,v in non_existing_needed_files.items():
            print('%s: %s' % (k, v))
        sys.exit(1)

def tier_needed_annotation_existing(invcf):
    # don't check for AF
    neededs = '1KG_AF UK10K_AF gnomAD_percent IMPACT_SNPEFF GERP CADD LowComplexRegion SegmentDup BP miRNA TF_binding'.split()
    info_tags_in_vcf = get_info_tags(invcf)

    # #test
    # print 'Info tags:'
    # for t in info_tags_in_vcf:
        # print t
    # print

    for tag in neededs:
        if tag not in info_tags_in_vcf:
            return False
    return True

def is_common_alt_in_at_least_two_datasets(k1g_af, uk10k_af, exac_af):
    cutoff = 0.1
    n = 0
    if k1g_af > cutoff:
        n += 1
    if uk10k_af > cutoff:
        n += 1
    if exac_af > cutoff:
        n += 1
    if n >= 2:
        return True
    else:
        return False

def get_min_frequency(multi_frequencies, star_in_alt):
    """
    to handle mult-ALT sites like UK10K_AF=0.247554,. or UK10K_AF=0.247554,0.1165
    if more than one, returns the minimal frequency in float value
    """
    frequencies = multi_frequencies.split(',')
    if star_in_alt: #In the 1-based position of star, there must be a '.'; It will be deleted
        del frequencies[star_in_alt -1]
    if '.' in frequencies:
        return 0
    return min([float(f) for f in frequencies])


def add_gnomad_to_vcf(infile, outfile, noMultiALT=False, debug=False):
    """
    modified from add_combined_population_fre_to_vcf()
    """
    needed_gnomad_keys = ['gnomAD_percent', 'gnomAD_HOM_ALT', 'gnomAD_filtered', 'gnomAD_AFR_pct',
        'gnomAD_AMR_pct', 'gnomAD_ASJ_pct', 'gnomAD_EAS_pct', 'gnomAD_FIN_pct',
        'gnomAD_NFE_pct', 'gnomAD_SAS_pct', 'gnomAD_OTH_pct' ]
    gnomad_col_str = ','.join(['CHROM', 'POS', 'REF', 'ALT'] + needed_gnomad_keys)

    if noMultiALT: #annotate the infile and get the outfile directly
        cmd = '''
        {BCFTOOLS} annotate \\
        -a {GNOMAD_FRE_FILE} \\
        --header-lines {GNOMAD_FRE_HEADER} \\
        -c {gnomad_col_str} \\
        {infile} \\
         > {outfile}
        '''.format(BCFTOOLS=BCFTOOLS, GNOMAD_FRE_FILE=GNOMAD_FRE_FILE,
                GNOMAD_FRE_HEADER=GNOMAD_FRE_HEADER,
                gnomad_col_str=gnomad_col_str,
                infile=infile, outfile=outfile)
        run_and_handle_returncode(cmd)
    else:
        vcfs_to_remove = []

        #extract the single-alt variants and annotate them with bcftools
        annotated_single_alt_vcf = random_file_at_same_dir(outfile, 'gnomad.annotated_single_alt', 'vcf.gz') #.vcf.gz
        vcfs_to_remove.append(annotated_single_alt_vcf)
        cmd = '''
        {BCFTOOLS} view -i 'N_ALT = 1' {infile} | \\
        {BCFTOOLS} annotate \\
        -a {GNOMAD_FRE_FILE} \\
        --header-lines {GNOMAD_FRE_HEADER} \\
        -c {gnomad_col_str} \\
        -O z \\
        - > {annotated_single_alt_vcf}
        '''.format(BCFTOOLS=BCFTOOLS, infile=infile, GNOMAD_FRE_FILE=GNOMAD_FRE_FILE,
                GNOMAD_FRE_HEADER=GNOMAD_FRE_HEADER,
                gnomad_col_str=gnomad_col_str,
                annotated_single_alt_vcf=annotated_single_alt_vcf)
        run_and_handle_returncode(cmd)
        tabix_on_bgzipped_vcf(annotated_single_alt_vcf)

        #extract the multi-alt variants, remove INFO, split and normalize, annotate the split VCF with bcftools, 
        # add the annotations back into the unannotated multi-alt VCF file
        #NOTE: the * should be handled properly

        #extract the multi-alt variants
        multi_alt_invcf = random_file_at_same_dir(outfile, 'gnomad.multi_alt_for_pop_fre', 'vcf') #.vcf
        vcfs_to_remove.append(multi_alt_invcf)
        cmd = '''
        {BCFTOOLS} view -i 'N_ALT > 1' \\
            {infile} \\
            > {outfile}
        '''.format(BCFTOOLS=BCFTOOLS, infile=infile, outfile=multi_alt_invcf)
        run_and_handle_error(cmd)

        #remove INFO, split and normalize
        split_alt_vcf = multi_alt_invcf[:-4] + '.split.vcf'
        vcfs_to_remove.append(split_alt_vcf)
        cmd = '''
        {BCFTOOLS} annotate -x INFO {multi_alt_invcf} |
        {VT} decompose -s - | \
        {VT} normalize -r {B37_REF} - > {split_alt_vcf}
        '''.format(BCFTOOLS=BCFTOOLS, VT=VT, multi_alt_invcf=multi_alt_invcf, B37_REF=B37_REF,
                split_alt_vcf=split_alt_vcf)
        run_and_handle_returncode(cmd)

        #annotate them with bcftools; removing the tags added by VT
        # keys_from_vt = ['OLD_MULTIALLELIC', 'OLD_VARIANT']
        annotated_split_vcf = split_alt_vcf[:-4] + '.annotated.vcf'
        vcfs_to_remove.append(annotated_split_vcf)
        cmd = '''
        {BCFTOOLS} annotate \\
        -a {GNOMAD_FRE_FILE} \\
        --header-lines {GNOMAD_FRE_HEADER} \\
        -c {gnomad_col_str} \\
        -x INFO/OLD_MULTIALLELIC,INFO/OLD_VARIANT \\
        {split_alt_vcf} \\
        > {annotated_split_vcf}
        '''.format(BCFTOOLS=BCFTOOLS, GNOMAD_FRE_FILE=GNOMAD_FRE_FILE,
                GNOMAD_FRE_HEADER=GNOMAD_FRE_HEADER,
                gnomad_col_str=gnomad_col_str,
                split_alt_vcf=split_alt_vcf,
                annotated_split_vcf=annotated_split_vcf)
        run_and_handle_error(cmd)

        #add the annotations back into the unannotated multi_alt_invcf file
        #FIXME add tmp__id (variant temp id) before splitting, and then use the tmp__id to map
        #the ALT allle between un-annoated and annoated VCF files, because with VT "Normalized variants may have their positions changed; in such cases, the normalized variants are reordered and output in an ordered fashion."
        id_to_annotate_pair = {}
        id_ = 0
        for line in get_vcf_iterator_in_plain_text(annotated_split_vcf):
            id_to_annotate_pair[id_] = {}
            info = line.rstrip().split('\t')[7]
            if info != '.':
                for pair in info.split(';'):
                    k,v = pair.split('=')
                    id_to_annotate_pair[id_][k] = v
            id_ += 1
        num_a_keys = needed_gnomad_keys
        annotated_multi_alt_vcf = multi_alt_invcf[:-4] + '.annotated.vcf'
        new_headers = open(GNOMAD_FRE_HEADER).readlines()
        outhandle_annotated_multi_alt_vcf = get_outhandle_with_new_headers(
                infile, annotated_multi_alt_vcf, *new_headers)
        id_ = 0
        for line in get_vcf_iterator_in_plain_text(multi_alt_invcf):
            elements = line.rstrip().split('\t')
            ids = []
            for _ in elements[4].split(','):
                ids.append(id_)
                id_ += 1
            alt_num = len(ids)
            k_v_pairs_to_add = []
            for k in num_a_keys:
                values = []
                for _id in ids:
                    try:
                        v = id_to_annotate_pair[_id][k]
                    except KeyError:
                        v = '.'
                    values.append(v)
                if not values.count('.') == alt_num:
                    k_v_pairs_to_add.append('%s=%s' % (k, ','.join(values)))
            if k_v_pairs_to_add:
                k_v_pair_str = ';'.join(k_v_pairs_to_add)
                if elements[7] != '.':
                    elements[7] = elements[7] + ';' + k_v_pair_str
                else:
                    elements[7] = k_v_pair_str
            outhandle_annotated_multi_alt_vcf.write('\t'.join(elements) + '\n')
        outhandle_annotated_multi_alt_vcf.close()

        create_bgzip_tabix_vcf(annotated_multi_alt_vcf)
        annotated_multi_alt_vcf = annotated_multi_alt_vcf +'.gz'
        vcfs_to_remove.append(annotated_multi_alt_vcf)

        # #combine the annotated single and multi alt files (both in .vcf.gz)
        #output to outfile

        #if the number of variants < 10K, use igvtools to sort the combined VCF file
        number_of_variants = count_variant_number(infile)
        if number_of_variants < 10 * 1000:
            combined_vcf = random_file_at_same_dir(outfile, 'tmp_gnomad_combined', 'vcf')
            vcfs_to_remove.append(combined_vcf)
        else:
            combined_vcf = outfile

        cmd = '''
        {BCFTOOLS} concat {annotated_single_alt_vcf} {annotated_multi_alt_vcf} \\
        --allow-overlaps --no-version > {outfile}
        '''.format(BCFTOOLS=BCFTOOLS, annotated_single_alt_vcf=annotated_single_alt_vcf,
                annotated_multi_alt_vcf=annotated_multi_alt_vcf, outfile=combined_vcf)
        run_and_handle_returncode(cmd)

        if number_of_variants < 10 * 1000:
            sort_vcf_by_igvtools(combined_vcf, outfile)

        if not debug:
            remove_vcf_file(*vcfs_to_remove)


def add_combined_population_fre_to_vcf(adds, infile, outfile, noMultiALT=False, debug=False):
    """
    infile is .vcf or .vcf.gz
    outfile is .vcf
    """

    CHROM, POS, REF, ALT = 'CHROM', 'POS', 'REF', 'ALT'
    if 'ESP' in adds:
        ESP_MAF_percent = 'ESP_MAF_percent'
    else:
        ESP_MAF_percent = '-'
    if '1KG' in adds:
        K1G_AF = '1KG_AF'
        K1G_AA = '1KG_AA'
    else:
        K1G_AF = '-'
        K1G_AA = '-'
    if 'UK10K' in adds:
        UK10K_AF = 'UK10K_AF'
    else:
        UK10K_AF = '-'
    if 'ExAC' in adds:
        ExAC_ALT_percent = 'ExAC_ALT_percent'
        ExAC_HOM_ALT = 'ExAC_HOM_ALT'
        ExAC_DP_mean = 'ExAC_DP_mean'
    else:
        ExAC_ALT_percent = '-'
        ExAC_HOM_ALT = '-'
        ExAC_DP_mean = '-'
    if 'nonTCGA-ExAC' in adds:
        ExAC_nonTCGA_ALT_percent = 'ExAC_nonTCGA_ALT_percent'
        ExAC_nonTCGA_HOM_ALT = 'ExAC_nonTCGA_HOM_ALT'
    else:
        ExAC_nonTCGA_ALT_percent = '-'
        ExAC_nonTCGA_HOM_ALT = '-'
    all_annot_keys = [ExAC_ALT_percent, K1G_AF, UK10K_AF, ESP_MAF_percent,
            ExAC_nonTCGA_ALT_percent, ExAC_HOM_ALT, ExAC_nonTCGA_HOM_ALT, ExAC_DP_mean, K1G_AA]
    columns = [CHROM, POS, REF, ALT] + all_annot_keys
    column_str = ','.join(columns)

    if noMultiALT: #annotate the infile and get the outfile directly
        cmd = '''
        {BCFTOOLS} annotate \\
        -a {POPULATION_FRE_FILE} \\
        --header-lines {POPULATION_FRE_HEADER} \\
        -c {column_str} \\
        {infile} \\
         > {outfile}
        '''.format(BCFTOOLS=BCFTOOLS, POPULATION_FRE_FILE=POPULATION_FRE_FILE,
                POPULATION_FRE_HEADER=POPULATION_FRE_HEADER,
                column_str=column_str,
                infile=infile, outfile=outfile)
        run_and_handle_returncode(cmd)
    else:
        vcfs_to_remove = []

        #extract the single-alt variants and annotate them with bcftools
        annotated_single_alt_vcf = random_file_at_same_dir(outfile, 'annotated_single_alt', 'vcf.gz') #.vcf.gz
        vcfs_to_remove.append(annotated_single_alt_vcf)
        cmd = '''
        {BCFTOOLS} view -i 'N_ALT = 1' {infile} | \\
        {BCFTOOLS} annotate \\
        -a {POPULATION_FRE_FILE} \\
        --header-lines {POPULATION_FRE_HEADER} \\
        -c {column_str} \\
        -O z \\
        - > {annotated_single_alt_vcf}
        '''.format(BCFTOOLS=BCFTOOLS, infile=infile, POPULATION_FRE_FILE=POPULATION_FRE_FILE,
                POPULATION_FRE_HEADER=POPULATION_FRE_HEADER,
                column_str=column_str,
                annotated_single_alt_vcf=annotated_single_alt_vcf)
        run_and_handle_returncode(cmd)
        tabix_on_bgzipped_vcf(annotated_single_alt_vcf)

        #extract the multi-alt variants, remove INFO, split and normalize, annotate the split VCF with bcftools, 
        # add the annotations back into the unannotated multi-alt VCF file
        #NOTE: the * should be handled properly

        #extract the multi-alt variants
        multi_alt_invcf = random_file_at_same_dir(outfile, 'multi_alt_for_pop_fre', 'vcf') #.vcf
        vcfs_to_remove.append(multi_alt_invcf)
        cmd = '''
        {BCFTOOLS} view -i 'N_ALT > 1' \\
            {infile} \\
            > {outfile}
        '''.format(BCFTOOLS=BCFTOOLS, infile=infile, outfile=multi_alt_invcf)
        run_and_handle_error(cmd)

        #remove INFO, split and normalize
        split_alt_vcf = multi_alt_invcf[:-4] + '.split.vcf'
        vcfs_to_remove.append(split_alt_vcf)
        cmd = '''
        {BCFTOOLS} annotate -x INFO {multi_alt_invcf} |
        {VT} decompose -s - | \
        {VT} normalize -r {B37_REF} - > {split_alt_vcf}
        '''.format(BCFTOOLS=BCFTOOLS, VT=VT, multi_alt_invcf=multi_alt_invcf, B37_REF=B37_REF,
                split_alt_vcf=split_alt_vcf)
        run_and_handle_returncode(cmd)

        #annotate them with bcftools; removing the tags added by VT
        # keys_from_vt = ['OLD_MULTIALLELIC', 'OLD_VARIANT']
        annotated_split_vcf = split_alt_vcf[:-4] + '.annotated.vcf'
        vcfs_to_remove.append(annotated_split_vcf)
        cmd = '''
        {BCFTOOLS} annotate \\
        -a {POPULATION_FRE_FILE} \\
        --header-lines {POPULATION_FRE_HEADER} \\
        -c {column_str} \\
        -x INFO/OLD_MULTIALLELIC,INFO/OLD_VARIANT \\
        {split_alt_vcf} \\
        > {annotated_split_vcf}
        '''.format(BCFTOOLS=BCFTOOLS, POPULATION_FRE_FILE=POPULATION_FRE_FILE,
                POPULATION_FRE_HEADER=POPULATION_FRE_HEADER,
                column_str=column_str,
                split_alt_vcf=split_alt_vcf,
                annotated_split_vcf=annotated_split_vcf)
        run_and_handle_error(cmd)

        #add the annotations back into the unannotated multi_alt_invcf file
        #FIXME add tmp__id (variant temp id) before splitting, and then use the tmp__id to map
        #the ALT allle between un-annoated and annoated VCF files, because with VT "Normalized variants may have their positions changed; in such cases, the normalized variants are reordered and output in an ordered fashion."
        id_to_annotate_pair = {}
        id_ = 0
        for line in get_vcf_iterator_in_plain_text(annotated_split_vcf):
            id_to_annotate_pair[id_] = {}
            info = line.rstrip().split('\t')[7]
            if info != '.':
                for pair in info.split(';'):
                    k,v = pair.split('=')
                    id_to_annotate_pair[id_][k] = v
            id_ += 1
        needed_keys = [k for k in all_annot_keys if k != '-']
        num_a_keys = [k for k in needed_keys if k in [K1G_AF, UK10K_AF, ExAC_ALT_percent,
                ExAC_HOM_ALT, ExAC_nonTCGA_ALT_percent, ExAC_nonTCGA_HOM_ALT]] #Number=A
        num_1_keys = [k for k in needed_keys if k in [ESP_MAF_percent, K1G_AA, ExAC_DP_mean]] #Number=. or Number=1; only the value from the first ALT will be used
        annotated_multi_alt_vcf = multi_alt_invcf[:-4] + '.annotated.vcf'
        new_headers = open(POPULATION_FRE_HEADER).readlines()
        outhandle_annotated_multi_alt_vcf = get_outhandle_with_new_headers(
                infile, annotated_multi_alt_vcf, *new_headers)
        id_ = 0
        for line in get_vcf_iterator_in_plain_text(multi_alt_invcf):
            elements = line.rstrip().split('\t')
            ids = []
            for _ in elements[4].split(','):
                ids.append(id_)
                id_ += 1
            alt_num = len(ids)
            k_v_pairs_to_add = []
            for k in num_a_keys:
                values = []
                for _id in ids:
                    try:
                        v = id_to_annotate_pair[_id][k]
                    except KeyError:
                        v = '.'
                    values.append(v)
                if not values.count('.') == alt_num:
                    k_v_pairs_to_add.append('%s=%s' % (k, ','.join(values)))
            for k in num_1_keys:
                for _id in ids:
                    try:
                        v = id_to_annotate_pair[_id][k]
                        k_v_pairs_to_add.append('%s=%s' % (k, v))
                        break
                    except KeyError:
                        continue
            if k_v_pairs_to_add:
                k_v_pair_str = ';'.join(k_v_pairs_to_add)
                if elements[7] != '.':
                    elements[7] = elements[7] + ';' + k_v_pair_str
                else:
                    elements[7] = k_v_pair_str
            outhandle_annotated_multi_alt_vcf.write('\t'.join(elements) + '\n')
        outhandle_annotated_multi_alt_vcf.close()

        create_bgzip_tabix_vcf(annotated_multi_alt_vcf)
        annotated_multi_alt_vcf = annotated_multi_alt_vcf +'.gz'
        vcfs_to_remove.append(annotated_multi_alt_vcf)

        # #combine the annotated single and multi alt files (both in .vcf.gz)
        #output to outfile

        #if the number of variants < 10K, use igvtools to sort the combined VCF file
        number_of_variants = count_variant_number(infile)
        if number_of_variants < 10 * 1000:
            combined_vcf = random_file_at_same_dir(outfile, 'tmp_popu_combined', 'vcf')
            vcfs_to_remove.append(combined_vcf)
        else:
            combined_vcf = outfile

        cmd = '''
        {BCFTOOLS} concat {annotated_single_alt_vcf} {annotated_multi_alt_vcf} \\
        --allow-overlaps --no-version > {outfile}
        '''.format(BCFTOOLS=BCFTOOLS, annotated_single_alt_vcf=annotated_single_alt_vcf,
                annotated_multi_alt_vcf=annotated_multi_alt_vcf, outfile=combined_vcf)
        run_and_handle_returncode(cmd)

        if number_of_variants < 10 * 1000:
            sort_vcf_by_igvtools(combined_vcf, outfile)

        if not debug:
            remove_vcf_file(*vcfs_to_remove)

High_impact = 'HIGH'
Moderate_impact = 'MODERATE'
Low_impact = 'LOW'
Modifier_impact = 'MODIFIER'

def parse_population_fre_for_tier(record, tag):
    """
    return one af and one maf
    """
    afs = record.INFO.get(tag)
    if not afs:
        return 0, 0
    else:
        alts = record.ALT
        if len(alts) > 1:
            if '*' in alts: #it crashes if there is only a "*" in the ALT column
                star_index = alts.index('*')
                afs = [v for v in afs if afs.index(v) != star_index]
            #replace None by 0
            afs = [af if af else 0 for af in afs]
            if tag.endswith('_percent'):
                afs = [af/100 for af in afs]
            af = min(afs)
            mafs = [1-f for f in afs]
            maf = min(afs + mafs)
        else:
            if tag.endswith('_percent'):
                afs = afs / 100
            af = afs
            maf = min(af, 1-af)
        return af, maf

def add_tier(invcf, outvcf):
    """
    invcf is .vcf or .vcf.gz
    outvcf is .vcf
    """
    #NOTE: currently we only store GERP >= 2.0 and CADD >= 10
    #NOTE: GERP already works well with deletion
    print('\nAdding SACGF Tiers...')
    outvcf = outvcf if not outvcf.endswith('.gz') else outvcf[:-3]
    outhandle = get_outhandle_with_new_headers(invcf, outvcf,
                        '##INFO=<ID=Tier,Number=1,Type=Integer,Description="SACGF Variant Tier">',
                        '##INFO=<ID=ExtremelyRare,Number=0,Type=Flag,Description="MAF < 0.05%% in 1KG, UK10K and gnomAD">',
                        '##INFO=<ID=Rare,Number=0,Type=Flag,Description="MAF < 0.5%% in 1KG, UK10K and gnomAD">'
                        )

    # AF = 'AF'
    K1G_AF = '1KG_AF'
    UK10K_AF = 'UK10K_AF'
    GNOMAD_AF = 'gnomAD_percent'
    ESP_MAF = 'ESP_MAF_percent'
    GERP = 'GERP'
    CADD = 'CADD'
    IMPACT_SNPEFF = 'IMPACT_SNPEFF'
    LowComplexRegion = 'LowComplexRegion'
    SegmentDup = 'SegmentDup'
    BP = 'BP'
    MIRNA = 'miRNA'
    TF_binding = 'TF_binding'

    for plain_line, record in zip(get_vcf_iterator_in_plain_text(invcf), cyvcf2.VCF(invcf)):
        #for multi-allele variants (including *), the min value is the population frequency
        k1g_af, k1g_maf = parse_population_fre_for_tier(record, K1G_AF)
        uk10k_af, uk10k_maf = parse_population_fre_for_tier(record, UK10K_AF)
        gnomad_af, gnomad_maf = parse_population_fre_for_tier(record, GNOMAD_AF)
        max_maf = max(k1g_maf, uk10k_maf, gnomad_maf)
        if max_maf < 0.0005:
            extremely_rare = True
            rare = True
        elif max_maf < 0.005:
            extremely_rare = False
            rare = True
        else:
            extremely_rare = False
            rare = False

        is_snp = record.var_type == 'snp'
        num_alt = len(record.ALT)

        impact = record.INFO.get(IMPACT_SNPEFF)
        gerp = record.INFO.get(GERP)
        cadd = record.INFO.get(CADD)
        low_complex_region = record.INFO.get(LowComplexRegion)
        segment_dup = record.INFO.get(SegmentDup)
        bp = record.INFO.get(BP)
        mirna = record.INFO.get(MIRNA)
        tf = record.INFO.get(TF_binding)

        tier = 6
        if (record.INFO.get('AF') == 1 or not record.INFO.get('AF')) and \
                (k1g_af > 0.5 or uk10k_af > 0.5 or gnomad_af > 0.5):
            extremely_rare = False
            rare = False
        else:
            if cadd and gerp:
                if not (record.ID or k1g_af or uk10k_af or gnomad_af or record.INFO.get(ESP_MAF)) and \
                        (impact in [High_impact, Moderate_impact] or bp or mirna or tf) and \
                        ((gerp > 4 or cadd > 30) or not is_snp) and \
                        not segment_dup and \
                        not (record.var_type == 'indel' and low_complex_region) and \
                        not (num_alt > 1):
                    tier = 1
                elif extremely_rare and \
                        (impact in [High_impact, Moderate_impact] or bp or mirna or tf) and \
                        ((gerp > 3 or cadd > 20) or not is_snp) and \
                        not segment_dup and \
                        not (record.var_type == 'indel' and low_complex_region) and \
                        not ( num_alt > 1):
                    tier = 2
                elif max_maf < 0.002 and \
                        (impact in [High_impact, Moderate_impact, Low_impact] or bp or mirna or tf) and \
                        ((gerp > 2 or cadd > 20) or not is_snp) and \
                        not segment_dup and \
                        not (record.var_type == 'indel' and low_complex_region) and \
                        not ( num_alt > 1):
                    tier = 3
                elif rare and \
                        (impact in [High_impact, Moderate_impact, Low_impact] or bp or mirna or tf) and \
                        ((gerp > 2 or cadd > 20) or not is_snp) and \
                        not (record.var_type == 'indel' and low_complex_region):
                    tier = 4
                elif max_maf < 0.01 and \
                        (impact in [High_impact, Moderate_impact, Low_impact] or bp or mirna or tf or \
                        (gerp > 2 or cadd > 20)):
                    tier = 5
                    
        #if it is a PASS or not
        if record.FILTER:
            tier += 6

        line_parts = plain_line.rstrip().split('\t')
        if extremely_rare:
            line_parts[7] = line_parts[7] + ';ExtremelyRare'
        if rare:
            line_parts[7] = line_parts[7] + ';Rare'
        line_parts[7] = line_parts[7] + ';Tier=%s' % tier
        outhandle.write('\t'.join(line_parts) + '\n')

    outhandle.close()

def infer_variant_interval(pos, ref, alt):
    """
    return the bed interval for the variant
    """
    start = pos - 1
    ref_len, alt_len = len(ref), len(alt)
    if ref_len == alt_len:
        end = start + ref_len
    elif ref_len < alt_len:
        start = start + ref_len - 1
        end = start + 1
    else:
        start = start + alt_len
        end = start + ref_len - alt_len 
    return start, end

def analyze_interval_values(intersects, index=None, select=None):
    """
    The select parameter determine what to return
    The index is for the annotation to extract
    """
    #remove records without annotations
    clean_intersects = intersects.filter(lambda x: x[4] != '.')

        #var_id must be converted to int
    if select == 'flag':
        var_set = set()
        for interset in clean_intersects:
            var_set.add(int(interset[3]))
            # import ipdb
            # ipdb.set_trace()
        return var_set
    else:
        var_to_values = {}
        for interset in clean_intersects:
            var_id = int(interset[3])
            value = interset[index]
            if value != '.': #I use '.' to indicat empty cell in the annotation database BED file
                try:
                    var_to_values[var_id].append(value)
                except KeyError:
                    var_to_values[var_id] = [value]
        final_var_to_value = {}
        if select == 'concat':
            for k, v in list(var_to_values.items()):
                final_var_to_value[k] = ','.join(sorted(list(set(v))))
            return final_var_to_value
        elif select == 'max':
            for k, v in list(var_to_values.items()):
                final_var_to_value[k] = max([float(i) for i in v])
            return final_var_to_value
        elif select == 'min':
            for k, v in list(var_to_values.items()):
                final_var_to_value[k] = min([float(i) for i in v])
            return final_var_to_value

def add_interval_annotation_by_pybedtools(invcf, outvcf, adds):
    '''
    Use pybedtools to annotate each record line
    invcf can be .vcf or .vcf.gz
    outvcf is .vcf
    '''
    invcf_intervals = []
    var_id = 0 #one var-ID for each record line
    for record in get_vcf_iterator_in_plain_text(invcf):
        chrom, pos, _, ref, alts = record.split('\t')[:5]
        pos = int(pos)
        # #test
        # if chrom == '1' and pos == 177927332:
            # import ipdb
            # ipdb.set_trace()
        if ',' not in alts:
            interval = infer_variant_interval(pos, ref, alts)
        else:
            starts = []
            ends = []
            for alt in alts.split(','):
                alt_interval = infer_variant_interval(pos, ref, alt)
                starts.append(alt_interval[0])
                ends.append(alt_interval[1])
            interval = min(starts), max(ends)
        invcf_intervals.append((chrom, interval[0], interval[1], var_id)) #var_id is always at index 3
        var_id += 1
    #sort invcf_intervals before converting it to bed
    #use 500-intervals window for sorting; Conting names in invcf must be sorted in karyotypic order beforehand
    sorted_intervals = []
    buffer_intervals = []
    count = 0
    chr_ = invcf_intervals[0][0]
    last_start = 0
    for interval in invcf_intervals:
        if chr_ != interval[0]:
            sorted_intervals.extend(buffer_intervals)
            buffer_intervals = [interval]
            chr_ = interval[0]
            last_start = interval[1]
            count = 1
            continue
        else:
            if count == 1000:
                sorted_intervals.extend(buffer_intervals[:500])
                buffer_intervals = buffer_intervals[500:]
                count = 500
            buffer_intervals.append(interval)
            count += 1
            if interval[1] < last_start:
                buffer_intervals = sorted(buffer_intervals, key=lambda v: v[1])
            else:
                last_start = interval[1]
    if buffer_intervals:
        sorted_intervals.extend(buffer_intervals)

    invcf_bed = BedTool(sorted_intervals)

    # # get the annotation for each record line
    # if 'GERP' in adds or 'branch-point' in adds or 'miRNA' in adds:

    if 'GERP' in adds or 'phyloP' in adds: # for both gerp and phyloP: 1  17462  17463 gerp_value phylop_value 
        gerp_phylop_interset = invcf_bed.intersect(GERP_n_PHYLOP_BED_FILE, loj=True, sorted=True, g=B37_GENOME_SIZE_FILE)
        if 'GERP' in adds:
            var_to_gerp = analyze_interval_values(gerp_phylop_interset, index=-2, select='max')
        if 'phyloP' in adds:
            var_to_phylop = analyze_interval_values(gerp_phylop_interset, index=-1, select='max')
        del gerp_phylop_interset
    if 'branch-point' in adds:
        branch_point_var_set = analyze_interval_values(
                invcf_bed.intersect(BRANCH_POINT_BED_FILE, loj=True, sorted=True, g=B37_GENOME_SIZE_FILE), select='flag')
    if 'miRNA' in adds:
        var_to_mirna = analyze_interval_values(
                invcf_bed.intersect(MIRNA_BINDING_FILE, loj=True, sorted=True, g=B37_GENOME_SIZE_FILE), index=-1, select='concat')

    # # add annotation into the invcf
    new_headers = []
    if 'GERP' in adds:
        new_headers.append('##INFO=<ID=GERP,Number=1,Type=Float,Description="Genomic Evolutionary Rate Profiling">')
    if 'phyloP' in adds:
        new_headers.append('##INFO=<ID=phyloP,Number=1,Type=Float,Description="phylogenetic p-value">')
    if 'branch-point' in adds:
        new_headers.append('##INFO=<ID=BP,Number=1,Type=Flag,Description="Branch point">')
    if 'miRNA' in adds:
        new_headers.append('##INFO=<ID=miRNA,Number=.,Type=String,Description="miRNA binding site as predicted by targetScan">')

    outhandle = get_outhandle_with_new_headers(invcf, outvcf, *new_headers)
    var_id = 0
    for record in get_vcf_iterator_in_plain_text(invcf):
        formated_info_to_add = []
        if 'GERP' in adds:
            if var_to_gerp.get(var_id):
                formated_info_to_add.append('GERP=%s' % var_to_gerp[var_id])
        if 'phyloP' in adds:
            if var_to_phylop.get(var_id):
                formated_info_to_add.append('phyloP=%s' % var_to_phylop[var_id])
        if 'branch-point' in adds:
            if var_id in branch_point_var_set:
                formated_info_to_add.append('BP')
        if 'miRNA' in adds:
            if var_to_mirna.get(var_id):
                formated_info_to_add.append('miRNA=%s' % var_to_mirna[var_id])
        var_id += 1
        outhandle.write(append_new_info(record, ';'.join(formated_info_to_add)))
    outhandle.close()

def add_annotation_by_bcftools(invcf, outvcf, adds):
    """
    invcf is .vcf. or .vcf.gz
    outvcf is .vcf
    """
    all_items = ['clinVar', 'TGI-tier', 'LCR', 'segment-dup', 'COSMIC', 'CADD',
            'AA-effects', 'dbSNP-ID'] #ordered: the less content the first to add, which is probably faster
    items_to_add = [i for i in all_items if i in adds]
    the_last_item = items_to_add[-1]

    command = ''
    if invcf.endswith('.vcf'):
        command += 'cat %s' % invcf
    else: #.vcf.gz
        command += '{bcftools} view {invcf}'.format(bcftools=BCFTOOLS, invcf=invcf)
    for item in items_to_add:
        if item != the_last_item:
            command += ' | {bcftools} annotate -Ou'.format(bcftools=BCFTOOLS) #using -Ou makes it slightly faster
        else:
            command += ' | {bcftools} annotate'.format(bcftools=BCFTOOLS)
        params = ''
        if item == 'clinVar':
            params += ' -a {CLINVAR_FILE} -h {CLINVAR_HEADER} -c CHROM,POS,REF,ALT,clinVar_disease'.format(
                    CLINVAR_FILE=CLINVAR_FILE, CLINVAR_HEADER=CLINVAR_HEADER)
        elif item == 'TGI-tier':
            params += ' -a {TGI_TIER_FILE} -h {TGI_TIER_HEADER} -c CHROM,FROM,TO,TGI_Tier'.format(
                    TGI_TIER_FILE=TGI_TIER_FILE, TGI_TIER_HEADER=TGI_TIER_HEADER)
        elif item == 'LCR':
            params += ' -a {LCR_FILE} -h {LCR_HEADER} -c CHROM,FROM,TO,LowComplexRegion'.format(
                    LCR_FILE=LCR_FILE, LCR_HEADER=LCR_HEADER)
        elif item == 'segment-dup':
            params += ' -a {SEGMENT_DUP_FILE} -h {SEGMENT_DUP_HEADER} -c CHROM,FROM,TO,SegmentDup'.format(
                    SEGMENT_DUP_FILE=SEGMENT_DUP_FILE, SEGMENT_DUP_HEADER=SEGMENT_DUP_HEADER)
        elif item == 'COSMIC':
            params += ' -a {COSMIC_FILE} -h {COSMIC_HEADER} -c CHROM,POS,REF,ALT,COSMIC_ID,COSMIC_PHE,COSMIC_CNT'.format(
                    COSMIC_FILE=COSMIC_FILE, COSMIC_HEADER=COSMIC_HEADER)
        elif item == 'CADD':
            params += ' -a {CADD_FILE} -h {CADD_HEADER} -c CHROM,POS,REF,ALT,CADD'.format(
                    CADD_FILE=CADD_FILE, CADD_HEADER=CADD_HEADER)
        elif item == 'AA-effects':
            params += ' -a {AA_EFF_PREDICT_FILE} -h {AA_EFF_PREDICT_HEADER} -c CHROM,POS,REF,ALT,SIFT,PolyPhen2,MutationTaster,FATHMM'.format(
                    AA_EFF_PREDICT_FILE=AA_EFF_PREDICT_FILE, AA_EFF_PREDICT_HEADER=AA_EFF_PREDICT_HEADER)
        elif item == 'dbSNP-ID':
            params += ' -a {DBSNP_FILE} -c CHROM,POS,REF,ALT,ID'.format(DBSNP_FILE=DBSNP_FILE)
        command += params

    command += ' > %s' % outvcf
    run_and_handle_error(command)


def test_java(javaXmx):
    ''' Crash early, crash often - Make sure we can allocate this much RAM by setting initial heap size '''
    java_Xms = '-Xms' + javaXmx
    # Use snpEff as java prints to stderr
    command = "java {java_Xms} -jar {SNPEFF_JAR} -version".format(java_Xms=java_Xms, SNPEFF_JAR=SNPEFF_JAR)
    run_and_handle_error(command)


if __name__ == '__main__':
    #invcf can be either .vcf or .vcf.gz (gzip or bgzipped)
    args = get_command_line_options()
    invcf = args.invcf
    outfile = args.outfile
    all_eff_out = args.all_eff_out
    skips = args.skip
    adds = args.add
    javaXmx = args.javaXmx
    debug = args.debug
    noMultiALT = args.noMultiALT

    invcf = absolute_file_path(invcf)
    outfile = absolute_file_path(outfile)
    outdir = os.path.dirname(outfile)

    adds = make_add_list(adds, skips)

    #validate invcf for vcf format and others
    err_message = validate_vcf_file(invcf)
    if err_message:
        if 'no record line' in err_message:
            #just bgziped and tabix the invcf of no variant record line; finish and return 0
            if invcf.endswith('.vcf'):
                create_bgzip_tabix_vcf(invcf, outfile)
                sys.exit(0)
            elif invcf.endswith('.vcf.gz'):
                run_and_handle_returncode('cp %s %s' % (invcf, outfile))
                tabix_on_bgzipped_vcf(outfile)
                sys.exit(0)
        else:
            # print a message and exit
            sys.exit('Variant annotation quit because: %s' %  err_message)
    # exit if the contig name starting with chr
    if get_the_first_variant(invcf).CHROM.startswith('chr'):
        sys.exit('Variant annotation quit because: %s' % 'Contig/chrom names start with "chr"')
    #
    check_needed_db_files_existing()
    needs_java = 'snpEFF' in adds or all_eff_out
    if needs_java:
        test_java(javaXmx)

    # #to guarantee contigs in the header lines for bcftools, or bcftools throw the error "contig '#' is not defined in the header"
    if invcf.endswith('.vcf.gz') or having_contig_in_header(invcf):
        last_outfile = invcf
    else:# .vcf without contigs in the header section
        last_outfile = invcf + '.gz'
        create_bgzip_tabix_vcf(invcf, last_outfile)

    #validate contigs are sorted in karyotypic order
    if having_contig_in_header(last_outfile):
        if not are_contigs_in_karyotypic_order(last_outfile):
            sys.exit('Variant annotation quit because: %s' % 'Contigs/chroms are not listed in karyotypic order in the header section.')

    # # bcftools streaming for:
    #clinVar, TGI-tier, LCR, segment-dup, COSMIC, CADD, AA-effects, dbSNP-ID
    if set(adds) & set(['dbSNP-ID', 'COSMIC', 'clinVar', 'LCR', 'segment-dup',
        'CADD', 'TGI-tier', 'AA-effects']):
        annotated_file = os.path.join(outdir, 'annotated_by_bcftools.%s.vcf' % random_file_name())
        add_annotation_by_bcftools(last_outfile, annotated_file, adds)
        if not debug and last_outfile != invcf:
            remove_vcf_file(last_outfile)
        last_outfile = annotated_file

    # # add population allele frequency
    if 'ESP' in adds or '1KG' in adds or 'UK10K' in adds or 'ExAC' in adds or 'nonTCGA-ExAC' in adds:
        annotated_file = random_file_at_same_dir(outfile, 'population_fre_added', 'vcf')
        add_combined_population_fre_to_vcf(adds, last_outfile, annotated_file, noMultiALT=noMultiALT, debug=debug)
        if not debug and last_outfile != invcf:
            remove_vcf_file(last_outfile)
        last_outfile = annotated_file

    if 'gnomAD' in adds:
        annotated_file = random_file_at_same_dir(outfile, 'gnomad_added', 'vcf')
        add_gnomad_to_vcf(last_outfile, annotated_file, noMultiALT=noMultiALT, debug=debug)
        if not debug and last_outfile != invcf:
            remove_vcf_file(last_outfile)
        last_outfile = annotated_file

    ## interval comparision
    if 'GERP' in adds or 'phyloP' in adds or 'branch-point' in adds or 'miRNA' in adds:
        annotated_file = random_file_at_same_dir(outfile, 'interval_compa', 'vcf')
        add_interval_annotation_by_pybedtools(last_outfile, annotated_file, adds)
        if not debug and last_outfile != invcf:
            remove_vcf_file(last_outfile)
        last_outfile = annotated_file

    # # run snpEFF eff, and extract the annotations I need
    if 'snpEFF' in adds or all_eff_out:
        annotated_file = random_file_at_same_dir(outfile, 'snpeff_added', 'vcf')
        add_snpeff_annotation(last_outfile, outdir, annotated_file, javaXmx, all_eff_out, invcf, debug)
        if not debug and last_outfile != invcf:
            remove_vcf_file(last_outfile)
        last_outfile = annotated_file

    #FIXME: remove this after the issue of ALT allele specific annotation is fixed by bcftools
    # This issue is already fixed for 1KG, UK10K, ESP, exAC and nonTCGA-ExAC
    # single ALT variant with a "*" can be annotated correctly
    if not noMultiALT and ('AA-effects' in adds or 'CADD' in adds):
        alt_specific_annot_items = 'CTRL_AC CTRL_AN CTRL_AF SIFT PolyPhen2 MutationTaster FATHMM CADD'.split()
        items_in_last_outfile = get_info_tags(last_outfile)
        items_to_remove = [tag for tag in items_in_last_outfile if tag in alt_specific_annot_items]
        if items_to_remove:
            outvcf_after_info_removed_at_multi_alt = random_file_at_same_dir(outfile, 'removed_info_at_multi_alt', 'vcf')
            remove_info_at_multi_alt_variants(last_outfile, outvcf_after_info_removed_at_multi_alt, items_to_remove, debug)
            if not debug and last_outfile != invcf:
                remove_vcf_file(last_outfile)
            last_outfile = outvcf_after_info_removed_at_multi_alt

    # add Tier
    if 'Tier' in adds and tier_needed_annotation_existing(last_outfile):
        annotated_file = random_file_at_same_dir(outfile, 'tier_added', 'vcf')
        add_tier(last_outfile, annotated_file)
        if not debug and last_outfile != invcf:
            remove_vcf_file(last_outfile)
        last_outfile = annotated_file

    # convert last_outfile (.vcf) to outfile (.vcf.gz)
    create_bgzip_tabix_vcf(last_outfile, outfile)
    if not debug and last_outfile != invcf:
        remove_vcf_file(last_outfile)
    print('\nVariant annotation was done. %s was created.' % outfile)
